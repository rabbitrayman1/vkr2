double BetaHyperbolicTanActivationFunction(double beta,
										double gain,
										double input)
{
	double res = (exp(2.0 * beta * input) - 1.0) / (exp(2.0 * beta * input) + 1.0);
	res = res * gain;
	return res;
}

void NeuralNetType1(__constant int* layers,
					__constant double* weights,
					__constant double* z,
					__global double* out)
{
	double allNeurons[nNeurons] = {0};
	for (int i = 0; i<num_of_inputs; i++)
	{
		allNeurons[i] = z[i];
	}
	int firstW = 0;
	int firstNeuron = 0;
	int firstBias = 0;
	for (int i = 1; i < nLayers; i++)
	{
		for (int l = 0; l<layers[i];l++)
		{	
			double sum = 0;
			for (int k = 0; k<layers[i-1];k++)
			{
				sum = sum + allNeurons[firstNeuron+k]*weights[firstW+k+l*layers[i-1]];
			}
			if (i == nLayers-1)
			{
				out[l] = BetaHyperbolicTanActivationFunction(2.0, 1.1, (sum+weights[SizeOfWeigths - (nBias - (firstBias+l))]));
			}
			else
			{
				allNeurons[firstNeuron+layers[i-1]+l] = BetaHyperbolicTanActivationFunction(2.0, 1.0, (sum+weights[SizeOfWeigths - (nBias - (firstBias+l))]));
			}
		}
		firstW = firstW + layers[i]*layers[i-1];
		firstNeuron = firstNeuron + layers[i-1];
		firstBias = firstBias + layers[i];
	}
}

void NeuralNetType2(__constant int* layers,
					__constant double* weights,
					__constant double* z,
					__global double* out)
{
	double prevNeurons[maxLayer] = {0};
	for (int i = 0; i<num_of_inputs; i++)
	{
		prevNeurons[i] = z[i];
	}
	int firstW = 0;
	int firstBias = 0;
	for (int i = 1; i < nLayers; i++)
	{
		double currNeurons[maxLayer] = {0};
		for (int l = 0; l<layers[i];l++)
		{
			double sum = 0;
			for (int k = 0; k<layers[i-1];k++)
			{
				sum = sum + prevNeurons[k]*weights[firstW+k+l*layers[i-1]];
			}
			if (i == nLayers-1)
			{
				out[l] = BetaHyperbolicTanActivationFunction(2.0, 1.1, (sum+weights[SizeOfWeigths - (nBias - (firstBias+l))]));
			}
			else
			{
				currNeurons[l] = BetaHyperbolicTanActivationFunction(2.0, 1.0, (sum+weights[SizeOfWeigths - (nBias - (firstBias+l))]));
			}
		}
		if (i != nLayers-1)
		{
			firstW = firstW + layers[i]*layers[i-1];
			for (int l = 0; l<layers[i];l++)
			{
				prevNeurons[l] = currNeurons[l];
			}
			firstBias = firstBias + layers[i];
		}
	}
}

__kernel void CalculateError(__global double* q,
						__constant double* w,
						__constant double* z,
						__constant int* layers,
						__constant double* expect)
{
	int global_id = get_global_id(0);
	if (global_id >= nPopulation) 
	{
		return;
	}
	for (int i = 0; i < num_z; i++)
	{
		if (NeuralType == 1)
			NeuralNetType1(&layers[0],&w[global_id*SizeOfWeigths],&z[i*num_of_inputs],&q[(global_id*num_z*num_of_outputs)+(i*num_of_outputs)]);
		else if (NeuralType == 2)
			NeuralNetType2(&layers[0],&w[global_id*SizeOfWeigths],&z[i*num_of_inputs],&q[(global_id*num_z*num_of_outputs)+(i*num_of_outputs)]);
		for (int j = 0; j < num_of_outputs; j++)
		{
			q[(global_id*num_z*num_of_outputs)+(i*num_of_outputs)+j] = pow((expect[(i*num_of_outputs)+j] - q[(global_id*num_z*num_of_outputs)+(i*num_of_outputs)+j]),2);
		}
	}
}

__kernel void maximum(__constant double* q,
                    __global double* v)
{
    int global_id = get_global_id(0);
    if (global_id >= nPopulation)
    {
        return;
    }
    for (int k = 0; k<num_of_outputs;k++)
    {
        int index = global_id * num_z * num_of_outputs + k;
        double max = q[index];
        for (int i = 1; i<num_z;i++)
        {
            index = index + num_of_outputs;
            if (max < q[index])
            {
                max=q[index];
            }
        }
        v[(global_id * num_of_outputs) + k] = max;
    }
}

bool fitness_logic(double a,
				double b)
{
	return (a>=b);
}

__kernel void paretoFitness(__global double* fit,
							__constant double* criteriaValues,
							int number)
{
	int global_id = get_global_id(0);
    if (global_id >= number)
    {
        return;
    }
	if(nPopulation == 1)
	{
	   fit[0] = 1;
	}
	else
	{
		double count = 0;
		double repmat[num_of_outputs] = {0};
		double d1 = 0;
		double d2 = 0;
		double num = 0;
		for (int j=0;j<num_of_outputs;j++)
		{
			repmat[j]=criteriaValues[(global_id*num_of_outputs)+j];
		}
		for (int k = 0; k<number;k++)
		{
			double c1 = 0;
			double c2 = 0;
			for (int j=0;j<num_of_outputs;j++)
			{
				double temp = repmat[j]-criteriaValues[(k*num_of_outputs)+j];
				c1 = c1 + fitness_logic(temp,0);
				if (temp==0)
					c2 = c2 + 1;
			}
			num = num + 1;
			if (c1 == num_of_outputs)
				d1 = d1 + 1;
			if (c2 == num_of_outputs)
				d2 = d2 + 1;
		}
		count = d1 - d2;
		fit[global_id] = pow( ( 1.0 + count/(num-1.0)),(-Q) );
	}
}

__kernel void selectionTournament(__constant double* population_fitness,
								__global ushort* parentsIndexes,
								__constant ushort* global_indexes)
{
	int global_id = get_global_id(0);
    if (global_id >= nPopulation)
    {
        return;
    }
	double tourParticipant[2] = {0};
	for (int i = 0; i<2;i++)
	{
		tourParticipant[i]=population_fitness[global_indexes[(global_id*2)+i]];
	}
    if ( tourParticipant[0]>=tourParticipant[1] )
		parentsIndexes[global_id] = global_indexes[(global_id*2)];
	else
		parentsIndexes[global_id] = global_indexes[(global_id*2)+1];
}


void realSBXCrossover(__constant double* parent1,
					__constant double* parent2,
					double* child1,
					double* child2,
					__constant ushort* k,
					__constant double* rand)
{
	for (int i = 0; i<k[0]; i++)
	{
		child1[i]=parent1[i];
		child2[i]=parent2[i];
	}
	
	for (int i = k[0]; i<dimensions; i++)
	{
		child1[i]=parent2[i];
		child2[i]=parent1[i];
	}
    
    double u = rand[0];
	
	double beta = 0;
	
    if(u<=0.5)
        beta = pow((2.0*u),(1.0/(crossover_param+1.0)));
    else
        beta = pow((1.0/(2.0*(1.0-u))),(1.0/(crossover_param+1.0)));
  
    child1[k[0]] = 0.5*((1.0+beta)*parent1[k[0]] + (1.0-beta)*parent2[k[0]]);
    child2[k[0]] = 0.5*((1.0-beta)*parent1[k[0]] + (1.0+beta)*parent2[k[0]]); 
}

void realRandomMutation(__constant ushort* r,
						double* child,
						__constant ushort* limits,
						__constant double* rand)
{
	double limit1 = limits[0];
	double limit2 = limits[1];
	child[r[0]] = limit1 + rand[0]*(limit2-limit1);
}


__kernel void newPopulation(__constant double* population,
							__global double* pNextPopulation,
							__constant ushort* parents,
							__constant double* rand, 
							__constant ushort* rand_k, 
							__constant ushort* limits)
{
	int global_id = get_global_id(0);
    if (global_id >= (nPopulation/2))
    {
        return;
    }
	double child1[dimensions] = {0};
	double child2[dimensions] = {0};
	int parent1 = parents[global_id*2]*dimensions;
	int parent2 = parents[(global_id*2)+1]*dimensions;
	if (rand[global_id*5]<=change_crossover)
		realSBXCrossover(&population[parent1],&population[parent2],&child1[0],&child2[0],&rand_k[global_id*3],&rand[(global_id*5)+3]);
	else
	{
		for (int i = 0; i < dimensions; i++)
		{
			child1[i]=population[parent1+i];
			child2[i]=population[parent2+i];
		}
	}
	if (rand[(global_id*5)+1] <= change_mutation)
		realRandomMutation(&rand_k[(global_id*3)+1],&child1[0],&limits[0],&rand[(global_id*5)+4]);

	if (rand[(global_id*5)+2] <= change_mutation)
		realRandomMutation(&rand_k[(global_id*3)+2],&child2[0],&limits[0],&rand[(global_id*5)+4]);
	
	for (int i=0;i<dimensions;i++)
	{
		pNextPopulation[(global_id*2*dimensions)+i] = child1[i];
		pNextPopulation[(((global_id*2)+1)*dimensions)+i] = child2[i];
	}
}
